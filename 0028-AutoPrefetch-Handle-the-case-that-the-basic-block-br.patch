From 3d20b13bc2e5af8d52e221a33881423e38c3dfdd Mon Sep 17 00:00:00 2001
From: dingguangya <dingguangya1@huawei.com>
Date: Thu, 17 Feb 2022 21:53:31 +0800
Subject: [PATCH 28/28] [AutoPrefetch] Handle the case that the basic block
 branch probability is invalid

 When the node branch probability value is not initialized,
 the branch probability must be set to 0 to ensure that
 the calculation of the basic block execution probability
 must be less than or equal to 100%.
---
 .../gcc.dg/autoprefetch/autoprefetch.exp      | 27 +++++++++++++++++++
 .../autoprefetch/branch-weighted-prefetch.c   | 22 +++++++++++++++
 .../autoprefetch/get-edge-prob-non-init.c     | 24 +++++++++++++++++
 gcc/tree-ssa-loop-prefetch.c                  | 17 +++++++++++-
 4 files changed, 89 insertions(+), 1 deletion(-)
 create mode 100644 gcc/testsuite/gcc.dg/autoprefetch/autoprefetch.exp
 create mode 100644 gcc/testsuite/gcc.dg/autoprefetch/branch-weighted-prefetch.c
 create mode 100644 gcc/testsuite/gcc.dg/autoprefetch/get-edge-prob-non-init.c

diff --git a/gcc/testsuite/gcc.dg/autoprefetch/autoprefetch.exp b/gcc/testsuite/gcc.dg/autoprefetch/autoprefetch.exp
new file mode 100644
index 000000000..a7408e338
--- /dev/null
+++ b/gcc/testsuite/gcc.dg/autoprefetch/autoprefetch.exp
@@ -0,0 +1,27 @@
+#   Copyright (C) 1997-2022 Free Software Foundation, Inc.
+
+# This program is free software; you can redistribute it and/or modify
+# it under the terms of the GNU General Public License as published by
+# the Free Software Foundation; either version 3 of the License, or
+# (at your option) any later version.
+# 
+# This program is distributed in the hope that it will be useful,
+# but WITHOUT ANY WARRANTY; without even the implied warranty of
+# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+# GNU General Public License for more details.
+# 
+# You should have received a copy of the GNU General Public License
+# along with GCC; see the file COPYING3.  If not see
+# <http://www.gnu.org/licenses/>.
+
+load_lib gcc-dg.exp
+load_lib target-supports.exp
+
+# Initialize `dg'.
+dg-init
+
+gcc-dg-runtest [lsort [glob -nocomplain $srcdir/$subdir/*.c]] \
+	"" "-fprefetch-loop-arrays"
+
+# All done.
+dg-finish
\ No newline at end of file
diff --git a/gcc/testsuite/gcc.dg/autoprefetch/branch-weighted-prefetch.c b/gcc/testsuite/gcc.dg/autoprefetch/branch-weighted-prefetch.c
new file mode 100644
index 000000000..c63c5e5cb
--- /dev/null
+++ b/gcc/testsuite/gcc.dg/autoprefetch/branch-weighted-prefetch.c
@@ -0,0 +1,22 @@
+/* { dg-do compile } */
+/* { dg-options "-O2 -fprefetch-loop-arrays=2 --param min-insn-to-prefetch-ratio=5 --param simultaneous-prefetches=100 -fdump-tree-aprefetch-details -fdump-tree-optimized" } */
+#define N 10000000
+
+long long a[N];
+
+long long func ()
+{
+  long long i;
+  long long sum = 0;
+
+  for (i = 0; i < N; i+=1) {
+	if (i < 100000)
+		sum += a[i];
+	else
+		continue;
+  }
+
+  return sum;
+}
+/* { dg-final { scan-tree-dump-times "Ahead 40" 1 "aprefetch" } } */
+/* { dg-final { scan-tree-dump-times "builtin_prefetch" 1 "optimized" } } */
\ No newline at end of file
diff --git a/gcc/testsuite/gcc.dg/autoprefetch/get-edge-prob-non-init.c b/gcc/testsuite/gcc.dg/autoprefetch/get-edge-prob-non-init.c
new file mode 100644
index 000000000..f55481008
--- /dev/null
+++ b/gcc/testsuite/gcc.dg/autoprefetch/get-edge-prob-non-init.c
@@ -0,0 +1,24 @@
+/* { dg-do compile } */
+/* { dg-options "-Ofast -fprefetch-loop-arrays=2 -fdump-tree-aprefetch-details" } */
+
+int a, c, f;
+static int *b = &a;
+int *d;
+int e[0];
+void g() {
+  int h;
+  for (;;) {
+    h = 1;
+    for (; h >= 0; h--) {
+      c = 2;
+      for (; c; c--)
+        if (e[0])
+          if (e[c])
+            *b = 0;
+      f || (*d = 0);
+    }
+  }
+}
+int main() {}
+
+/* { dg-final } */
diff --git a/gcc/tree-ssa-loop-prefetch.c b/gcc/tree-ssa-loop-prefetch.c
index 3a5aef0fc..673f453a4 100644
--- a/gcc/tree-ssa-loop-prefetch.c
+++ b/gcc/tree-ssa-loop-prefetch.c
@@ -2132,7 +2132,7 @@ get_edge_prob (edge e)
 {
   /* Limit the minimum probability value.  */
   const float MINNUM_PROB = 0.00001f;
-  float fvalue = 1;
+  float fvalue = 0;
 
   profile_probability probability = e->probability;
   if (probability.initialized_p ())
@@ -2143,6 +2143,21 @@ get_edge_prob (edge e)
 	  fvalue = MINNUM_PROB;
 	}
     }
+  else
+    {
+      /* When the node branch probability value is not initialized, the branch
+	 probability must be set to 0 to ensure that the calculation of the
+	 basic block execution probability must be less than or equal to 100%.
+	 i.e,
+	 ...
+	 <bb 3> [local count: 20000]
+	 if (f_2 != 0)
+	   goto <bb 6>; [INV]
+	 else
+	   goto <bb 7>; [100.00%]
+	 ...  */
+      fvalue = 0;
+    }
   return fvalue;
 }
 
-- 
2.27.0.windows.1

